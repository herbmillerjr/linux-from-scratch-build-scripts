## test for package existence
if [ ! -f dejagnu-1.5.tar.gz ];
then
	wget http://ftp.gnu.org/gnu/dejagnu/dejagnu-1.5.tar.gz
fi

## nuke
rm -rf dejagnu-1.5

## restart
tar -zxf dejagnu-1.5.tar.gz
cd dejagnu-1.5

## dig in
./configure --prefix=/tools

make install 