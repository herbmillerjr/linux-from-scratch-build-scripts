if [ ! -f binutils-2.21.1a.tar.bz2 ];
then
	wget http://ftp.gnu.org/gnu/binutils/binutils-2.21.1a.tar.bz2
fi
rm -rf binutils-2.21.1 binutils-build
tar -jxf binutils-2.21.1a.tar.bz2
mkdir binutils-build
cd binutils-build

../binutils-2.21.1/configure \
    --target=$LFS_TGT --prefix=/tools \
    --disable-nls --disable-werror

make

case $(uname -m) in
  x86_64) mkdir -v /tools/lib && ln -sv lib /tools/lib64 ;;
esac

make install

