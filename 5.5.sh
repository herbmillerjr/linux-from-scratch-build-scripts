## gcc itself
if [ ! -f gcc-4.6.1.tar.bz2 ];
then
	wget http://ftp.gnu.org/gnu/gcc/gcc-4.6.1/gcc-4.6.1.tar.bz2
fi

## additional packages it relies on nowadays
if [ ! -f gmp-5.0.2.tar.bz2 ];
then
	wget http://ftp.gnu.org/gnu/gmp/gmp-5.0.2.tar.bz2
fi
if [ ! -f mpfr-3.1.0.tar.bz2 ];
then
	wget http://www.mpfr.org/mpfr-3.1.0/mpfr-3.1.0.tar.bz2
fi
if [ ! -f mpc-0.9.tar.gz ];
then
	wget http://www.multiprecision.org/mpc/download/mpc-0.9.tar.gz
fi

## patches
if [ ! -f gcc-4.6.1-cross_compile-1.patch ];
then
	wget http://www.linuxfromscratch.org/patches/lfs/7.0/gcc-4.6.1-cross_compile-1.patch
fi

## ALWAYS CLEAN UP FIRST!
rm -rf gcc-4.6.1
rm -rf gcc-build

tar -jxf gcc-4.6.1.tar.bz2
cd gcc-4.6.1

tar -jxf ../mpfr-3.1.0.tar.bz2
mv -v mpfr-3.1.0 mpfr
tar -jxf ../gmp-5.0.2.tar.bz2
mv -v gmp-5.0.2 gmp
tar -zxf ../mpc-0.9.tar.gz
mv -v mpc-0.9 mpc

patch -Np1 -i ../gcc-4.6.1-cross_compile-1.patch

mkdir -v ../gcc-build
cd ../gcc-build

../gcc-4.6.1/configure \
    --target=$LFS_TGT --prefix=/tools \
    --disable-nls --disable-shared --disable-multilib \
    --disable-decimal-float --disable-threads \
    --disable-libmudflap --disable-libssp \
    --disable-libgomp --disable-libquadmath \
    --disable-target-libiberty --disable-target-zlib \
    --enable-languages=c --without-ppl --without-cloog \
    --with-mpfr-include=$(pwd)/../gcc-4.6.1/mpfr/src \
    --with-mpfr-lib=$(pwd)/mpfr/src/.libs

make

make install

ln -vs libgcc.a `$LFS_TGT-gcc -print-libgcc-file-name | \
    sed 's/libgcc/&_eh/'`

