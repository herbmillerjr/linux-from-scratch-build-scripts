## kernel tarball
if [ ! -f linux-3.1.tar.bz2 ]
then
	wget http://www.kernel.org/pub/linux/kernel/v3.x/linux-3.1.tar.bz2
fi

rm -rf linux-3.1

tar -jxf linux-3.1.tar.bz2
cd linux-3.1

make mrproper

make headers_check
make INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* /tools/include

