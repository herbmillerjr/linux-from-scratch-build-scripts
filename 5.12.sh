## test of package existence
if [ ! -f expect5.45.tar.gz ];
then
	wget http://prdownloads.sourceforge.net/expect/expect5.45.tar.gz
fi

## blow up remains
rm -rf expect5.45

tar -zxf expect5.45.tar.gz
cd expect5.45

cp -v configure{,.orig}
sed 's:/usr/local/bin:/bin:' configure.orig > configure

./configure --prefix=/tools --with-tcl=/tools/lib --with-tclinclude=/tools/include
  
make

make test

make SCRIPTS="" install
