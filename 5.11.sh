## check for package
if [ ! -f tcl8.5.10-src.tar.gz ];
then
	wget http://prdownloads.sourceforge.net/tcl/tcl8.5.10-src.tar.gz
fi

## nuke leftovers
rm -rf tcl8.5.10

tar -zxf tcl8.5.10-src.tar.gz
cd tcl8.5.10

## begin
cd unix
./configure --prefix=/tools

make

make install

chmod -v u+w /tools/lib/libtcl8.5.so

make install-private-headers

ln -sv tclsh8.5 /tools/bin/tclsh

