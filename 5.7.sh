## check for glibc itself
if [ ! -f glibc-2.14.1.tar.bz2 ];
then
	wget http://ftp.gnu.org/gnu/glibc/glibc-2.14.1.tar.bz2
fi

## check for patches
if [ ! -f glibc-2.14.1-gcc_fix-1.patch ];
then
	wget http://www.linuxfromscratch.org/patches/lfs/7.0/glibc-2.14.1-gcc_fix-1.patch
fi
if [ ! -f glibc-2.14.1-cpuid-1.patch ];
then
	wget http://www.linuxfromscratch.org/patches/lfs/7.0/glibc-2.14.1-cpuid-1.patch
fi

## NUKE REMAINS FROM PREVIOUS ATTEMPTS
rm -rf glibc-2.14.1 glibc-build

## setup
tar -jxf glibc-2.14.1.tar.bz2
cd glibc-2.14.1

## patch
patch -Np1 -i ../glibc-2.14.1-gcc_fix-1.patch
patch -Np1 -i ../glibc-2.14.1-cpuid-1.patch

## get to it
mkdir -v ../glibc-build
cd ../glibc-build

../glibc-2.14.1/configure --prefix=/tools \
    --host=$LFS_TGT --build=$(../glibc-2.14.1/scripts/config.guess) \
    --disable-profile --enable-add-ons \
    --enable-kernel=2.6.25 --with-headers=/tools/include \
    libc_cv_forced_unwind=yes libc_cv_c_cleanup=yes

make

make install

