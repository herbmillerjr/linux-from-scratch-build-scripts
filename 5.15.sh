## check for existence
if [ ! -f ncurses-5.9.tar.gz ];
then
	wget ftp://ftp.gnu.org/gnu/ncurses/ncurses-5.9.tar.gz
fi

## nuke
rm -rf ncurses-5.9

## rebuild
tar -zxf ncurses-5.9.tar.gz
cd ncurses-5.9

## oh yeah
./configure --prefix=/tools --with-shared --without-debug --without-ada --enable-overwrite

make

make install
