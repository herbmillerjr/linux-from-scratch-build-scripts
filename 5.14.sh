## usual
if [ ! -f check-0.9.8.tar.gz ];
then
	wget http://sourceforge.net/projects/check/files/check/0.9.8/check-0.9.8.tar.gz
fi

## nuke
rm -rf check-0.9.8

## restart
tar -zxf check-0.9.8.tar.gz
cd check-0.9.8

## let the fun begin
./configure --prefix=/tools

make

make install
