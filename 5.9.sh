## not checking for package by this point becuase
## how did we build it in the first place if we
## didn't have it

## NUKE REMNANTS
rm -rf binutils-2.21.1 binutils-build

## start fresh
tar -jxf binutils-2.21.1a.tar.bz2

mkdir binutils-build
cd binutils-build

CC="$LFS_TGT-gcc -B/tools/lib/" \
   AR=$LFS_TGT-ar RANLIB=$LFS_TGT-ranlib \
   ../binutils-2.21.1/configure --prefix=/tools \
   --disable-nls --with-lib-path=/tools/lib

make

make install

make -C ld clean
make -C ld LIB_PATH=/usr/lib:/lib
cp -v ld/ld-new /tools/bin

